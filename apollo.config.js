module.exports = {
    client: {
        service: {
            name: "graphql",
            url: "http://45.173.129.195:8000/graphql",
            // optional disable SSL validation check      
            skipSSLValidation: true
        }
    }
};