import { createStore, combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import reducer from './Reducers'


const store = createStore(
    combineReducers({
        contador:reducer,
        form:formReducer
    })
    )

export default store