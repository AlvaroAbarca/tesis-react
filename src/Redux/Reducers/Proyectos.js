const ADD_PROYECTO = 'proyecto/add'

/*
* id
* name
* about
* 
*/

const addProyecto = payload => ({
    type: ADD_PROYECTO,
    payload:{
        ...payload,
        id: Math.random.toString(36)
    }
})
const initialState = {
    data:[]
}
export default function reducer(state = initialState, action){
    switch (action.type) {
        case ADD_PROYECTO:
            return {
                ...state,
                data: [...state.data, action.payload],
            }
        default:
            return state;
    }
}