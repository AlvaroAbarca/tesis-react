const ADD_MAPA = 'mapa/add'

/*
* id
* name
* about
* category_id
*/

const addMapa = payload => ({
    type: ADD_MAPA,
    payload:{
        ...payload,
        id: Math.random.toString(36)
    }
})
const initialState = {
    data:[{
        id:1,
        name:'Defecto'
    }]
}
export default function reducer(state = initialState, action){
    switch (action.type) {
        case ADD_MAPA:
            return {
                ...state,
                data: [...state.data, action.payload],
            }
        default:
            return state;
    }
}