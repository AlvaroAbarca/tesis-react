import React, { Component } from 'react'
import styled from 'styled-components'

import { Alert } from 'react-bootstrap'

const InputStyled = styled.input`

`

class InputForm extends Component {
    render() {
        const { input, meta } = this.props
        return (
            <div>
                {meta.submitFailed && meta.error && <Alert variant="danger">{meta.error}</Alert>}
                <InputStyled {...input} {...this.props} />
            </div>
        )
    }
}

export default InputForm
