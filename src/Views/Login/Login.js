import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import InputForm from './InputForm'

import StyledLogin from '../../Styles/StyledLogin'
import { Form, Image, Button } from 'react-bootstrap'

const validate = values =>{
    const errors = {}
    if(!values.email){
        errors.email = 'Campo Obligatorio'
    }
    if(!values.password){
        errors.password = 'Campo Obligatorio'
    }

    return errors
}

class Login extends Component {
    state = {
        email:'',
        password:''
    }
    setUser = ({ target }) =>{
        const { name, value } = target
        this.setState({ [name]:value })
    }
    handleSubmit(e){
        console.log(e)
    }
    render(){
        //const { email, password } = this.state
        //const { handleSubmit } = this.props
        //console.log(email, password)
        return(
            <StyledLogin className="text-center" >
                <Form className="form-signin" onSubmit={ this.handleSubmit } >
                    <Image className="mb-4" src="https://getbootstrap.com/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
                    <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                    <label htmlFor="inputEmail" className="sr-only">Email address</label>
                    <Field type="email" name="email" id="inputEmail" className="form-control" placeholder="Email address" required autoFocus onChange={ this.setUser } component={InputForm} />
                    <label htmlFor="inputPassword" className="sr-only">Password</label>
                    <Field type="password" name="password" id="inputPassword" className="form-control" placeholder="Password" required onChange={ this.setUser } component={InputForm}/>
                    <div className="checkbox mb-3">
                        <label>
                            <input type="checkbox" value="remember-me" /> Remember me
                        </label>
                    </div>
                    <Button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</Button>
                    <p className="mt-5 mb-3 text-muted">&copy; 2017-2019</p>
                </Form>
            </StyledLogin>
        )
    }
}
export default reduxForm({
    form:'Login',
    validate
})(Login)