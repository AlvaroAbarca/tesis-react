import React from 'react';
import { Switch, Route } from 'react-router-dom'
import '../App.css'

// Import Component
import NAV from '../Components/NAV'
import Online from '../Components/Online'
import HomeContent from '../Components/Home/Home'



const state = {
    status: false
}

const Home = () => {
    const { status } = state;
    return(
        <>
            <header>
                <NAV user={'Alvaro A'} />
            </header>
            <Switch>
                <Route exact path="home/" component={HomeContent} />
                <Route exact path="/" component={HomeContent} />
            </Switch>
            <footer>
                <Online status={ status } />
            </footer>
        </>
    )
}

export default Home