import React, { PureComponent } from 'react'
import { Container, Row, Col } from 'react-bootstrap'

import StyledTitle from '../../Styles/StyledTitle'
import MyProjects from '../Projects/MyProjects'
import LastChanges from '../Notification/LastChanges'

const list = [
    {  
        id:1,
        title:'Titulo 1',
        text: 'Texto 1',
        url:'https://www.facebook.com/'
    }
];

class HomeContent extends PureComponent {
    render(){
        return(
            <Container>
                <Row className="App">
                    <Col xs={8}>
                        <Row>
                            <Col xs={8}>
                                <StyledTitle className="text-left">My Projects</StyledTitle>
                            </Col>
                            <Col xs={4}>

                            </Col>
                        </Row>
                        <Row>
                            <Col xs={4} className="pl-0 pr-1">
                                <MyProjects />
                            </Col>
                            <Col xs={4} className="px-1">
                                <MyProjects />
                            </Col>
                            <Col xs={4} className="pl-1 pr-0">
                                <MyProjects />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={8}>
                                <StyledTitle className="text-left">Last Maps</StyledTitle>
                            </Col>
                        </Row>
                        <Row>

                        </Row>
                    </Col>
                    <Col xs={4} lg={4}>
                        <LastChanges list={ list }></LastChanges>
                    </Col>
                </Row>
                <Row>
                <Col>
                </Col>
                </Row>
            </Container>
        )
    }
}

export default HomeContent