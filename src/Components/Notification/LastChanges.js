import React, { Component } from 'react'
import { ListGroup } from 'react-bootstrap'
import CARD from './CARD'

class ListMaps extends Component {
  render() {
    const { list } = this.props
    return (
        <>
          <h3 className="text-left" style={{ textDecoration:"underline"}} >Last Changes      </h3>
          <ListGroup as="ul">
              {list.map(item => <ListGroup.Item key={item.id} as="li"><CARD item={item}/></ListGroup.Item>)}
          </ListGroup>
        </>
    )
  }
}

export default ListMaps

// Component for last Maps or Last Changes