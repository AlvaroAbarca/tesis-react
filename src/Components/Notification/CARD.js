import React from "react";
import { Card } from "react-bootstrap";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGlobeAmericas } from '@fortawesome/free-solid-svg-icons'

import PropTypes from 'prop-types'

const CARD = (props) => {
    const { title, text, url } = props.item
    return (
        <Card style={{  }}> 
        {/* width: "18rem" */}
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{text}</Card.Subtitle>
                <Card.Text>{text}</Card.Text>
                <Card.Link href={url} className="float-left "><FontAwesomeIcon icon={faGlobeAmericas} /> Go To Map</Card.Link>
            </Card.Body>
        </Card>
    );
}

CARD.propTypes = {
    item:{
        title: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired
    }
}

export default CARD