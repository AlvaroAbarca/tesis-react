import React from 'react'
// import PropTypes from 'prop-types'

// import styled from 'styled-components'
import { Card } from 'react-bootstrap'

const MyProjects = () => {
    return(
        <>
            <Card className="" >
                <Card.Body className="py-10">
                    <Card.Title style={{ textDecoration:"underline" }} className="text-left">Landslide prevention Chaiten</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Participants: Nelson, Jonanthan, Sergio</Card.Subtitle>
                    <Card.Text>Determining dangerous places due to landslide possibilities in Chaiten and preparation works to mitigate it's consequences</Card.Text>
                </Card.Body>

            </Card>
        </>
    )
}
export default MyProjects