import React from "react";
// eslint-disable-next-line
import { Navbar, Nav, Form, FormControl, Button, Container, Col, Row, Badge, NavDropdown } from "react-bootstrap";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBell, faEnvelope, faUser, faSmileBeam } from '@fortawesome/free-solid-svg-icons';

import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

const NAV = (props) => {
    const { user = 'Usuario' } = props
    return(
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" >
            <Container>
                <Link to="/" className="navbar-brand" activeClassName="hurray">
                    <FontAwesomeIcon icon={ faSmileBeam } />
                </Link>
                {/* <Navbar.Brand>
                </Navbar.Brand> */}
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Link to="/" className="nav-link" activeClassName="hurray">
                        Home
                        </Link>
                        <Link to="/" className="nav-link" activeClassName="hurray">
                            Projects
                        </Link>
                    </Nav>
                    <Nav>
                        <Nav.Link href="#deets">
                            <Badge pill variant="danger" className="align-top mx-0" >1</Badge>
                            <FontAwesomeIcon icon={ faBell } />
                        </Nav.Link>
                        <Nav.Link href="#deets">
                            <Badge pill variant="danger" className="align-top mx-0" >1</Badge>
                            <FontAwesomeIcon icon={ faEnvelope } />
                        </Nav.Link>
                        <Nav.Link href="#deets">
                            <FontAwesomeIcon icon={ faUser } style={{ marginRight:"5px" }} />
                            { user }
                        </Nav.Link>
                        <NavDropdown title={ user } id="basic-nav-dropdown">
                            <NavDropdown.Item>
                                <FontAwesomeIcon icon={ faUser } style={{ marginRight:"5px" }} />
                                { user }
                            </NavDropdown.Item>
                            <NavDropdown.Item>Cerrar Sesion</NavDropdown.Item>
                        </NavDropdown>
                        {/* <Nav.Link eventKey={2} href="#memes">
                            { user }
                        </Nav.Link> */}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

NAV.propTypes = {
    user: PropTypes.string.isRequired
}

export default NAV;