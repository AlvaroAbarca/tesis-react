import React from 'react';

import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledButton = styled.button`
    font-size: 1.2em;
    margin: 1em 1em 0 1em;
    position: fixed;
    bottom: 0;
    right: 20%;
    background-color: #ffffff;
    border-top: 2px solid #000;
    border-left: 2px solid #000;
    border-right: 2px solid #000;
    border-start-start-radius: 5px;
    border-start-end-radius: 5px;
`;
const StyledCircle = styled.span`
    height: 15px;
    width: 15px;
    background-color: ${props => props.status ? "red" : "green" };
    border-radius: 50%;
    display: inline-block;
`;

const Online = () => {
    return(
        <StyledButton className="btn"><StyledCircle /> Online</StyledButton>
        // <Button bg="dark" variant="dark" className="" style={}> Online</Button>
    )
};

Online.propTypes = {
    status: PropTypes.bool.isRequired
}

export default Online