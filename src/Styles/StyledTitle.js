import styled from 'styled-components'

const StyledTitle = styled.h2`
    margin-top: 10px;
    text-decoration:underline;
`
export default StyledTitle