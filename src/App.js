import React from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux'
//import MapDetail from './Views/Maps/MapDetail'
import Viewrouter from './Views/View-router';
import NoMatch from './Views/NoMatch';
import Login from './Views/Login/Login';

import NAV from './Components/NAV'
import Online from './Components/Online'
import HomeContent from './Components/Home/Home'

const state = {
  status: false
}
const DefaultLayout = ({component: Component, status,...rest}) => {
  return (
    <Route {...rest} render={matchProps => (
      <div className="DefaultLayout">
          <header>
            <NAV user={'Alvaro A'} />
          </header>
          <Component {...matchProps} />
          <footer>
            <Online status={ status } />
          </footer>
      </div>
    )} />
  )
}

function App() {
  const { status } = state;
  return (
    <>
      <Switch>
        <DefaultLayout exact path="/" component={HomeContent} />
        <Route exact path="/login" component={Login} />
        <Route component={NoMatch} />
      </Switch>
    </>
  );
}
const mapStateToProps = state =>{
  console.log(state)
  return state
}
const mapDispatchToProps = dispatch =>({})
export default connect(mapStateToProps, mapDispatchToProps)(App);
